package com.eva.service.system;

import com.eva.core.model.PageData;
import com.eva.core.model.PageWrap;
import com.eva.dao.system.dto.QuerySystemTraceLogDTO;
import com.eva.dao.system.model.SystemTraceLog;
import java.util.List;

/**
 * 跟踪日志Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemTraceLogService {

    /**
     * 创建
     *
     * @param systemTraceLog 实体
     * @return Integer
     */
    Integer create(SystemTraceLog systemTraceLog);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 批量主键删除
     *
     * @param ids 主键列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     *
     * @param systemTraceLog 实体
     */
    void updateById(SystemTraceLog systemTraceLog);

    /**
     * 批量主键更新
     *
     * @param systemTraceLogs 实体列表
     */
    void updateByIdInBatch(List<SystemTraceLog> systemTraceLogs);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemTraceLog
     */
    SystemTraceLog findById(Integer id);

    /**
     * 条件查询单条记录
     *
     * @param systemTraceLog 查询条件
     * @return SystemTraceLog
     */
    SystemTraceLog findOne(SystemTraceLog systemTraceLog);

    /**
     * 条件查询
     *
     * @param systemTraceLog 查询条件
     * @return List<SystemTraceLog>
     */
    List<SystemTraceLog> findList(SystemTraceLog systemTraceLog);

    /**
     * 分页查询
     *
     * @param pageWrap 分页参数
     * @return PageData<SystemTraceLog>
     */
    PageData<SystemTraceLog> findPage(PageWrap<QuerySystemTraceLogDTO> pageWrap);

    /**
     * 条件统计
     *
     * @param systemTraceLog 统计参数
     * @return long
     */
    long count(SystemTraceLog systemTraceLog);
}
